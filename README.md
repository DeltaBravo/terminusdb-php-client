# TerminusDB PHP Client

TerminusDB client written in PHP for use with PHP

## Requirements

- [TerminusDB](https://github.com/terminusdb/terminusdb)
- [PHP 8+](https://www.php.net/downloads)

[comment]: <> (## Installation)

[comment]: <> (## Usage)

[comment]: <> (## API)

[comment]: <> (## Report Issues)

[comment]: <> (## Contribute)

## Licence

GNU General Public License (GPLv3)

© Copyright 2021 Daniel Baer
