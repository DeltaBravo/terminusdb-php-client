<?php

namespace TerminusDB;

use JsonException;
use Psr\Log\{LoggerAwareInterface, LoggerInterface};
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\{
    ClientExceptionInterface,
    DecodingExceptionInterface,
    RedirectionExceptionInterface,
    ServerExceptionInterface,
    TransportExceptionInterface
};
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TerminusDB implements LoggerAwareInterface
{
    private string              $database;
    private HttpClientInterface $client;
    private LoggerInterface     $logger;

    public function __construct()
    {
        $this->setClient();
    }

    /**
     * Submits query to TerminusDB and returns results.
     *
     * Successful query with an empty result set returns null.
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws JsonException
     */
    public function query(string $type, array $queryParts): ?array
    {
        if (!$this->client) {
            $this->logger->critical(
                'Not connected to database. Call TerminusDB->setClient() prior to calling other TerminusDB methods.'
            );

            return null;
        }

        $queryTypes = ['And', 'GroupBy', 'OrderBy', 'Select', 'Triple', 'Quad'];

        if (!in_array($type, $queryTypes)) {
            $queryTypesString = implode(', ', $queryTypes);

            $this->logger->critical("Unknown query type \"$type\". Must be one of $queryTypesString.");

            return null;
        }

        $queryBase = [
            '@type'    => "woql:{$type}",
            '@context' => (object) [
                'layer'  => 'http://terminusdb.com/schema/layer#',
                'owl'    => 'http://www.w3.org/2002/07/owl#',
                'rdf'    => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
                'rdfs'   => 'http://www.w3.org/2000/01/rdf-schema#',
                'ref'    => 'http://terminusdb.com/schema/ref#',
                'repo'   => 'http://terminusdb.com/schema/repository#',
                'scm'    => 'terminusdb:///schema#',
                'system' => 'http://terminusdb.com/schema/system#',
                'woql'   => 'http://terminusdb.com/schema/woql#',
                'xdd'    => 'http://terminusdb.com/schema/xdd#',
                'xsd'    => 'http://www.w3.org/2001/XMLSchema#',
                'vio'    => 'http://terminusdb.com/schema/vio#',
                '_'      => '_:',
            ],
        ];

        $queryFull    = (object) [
            // #HERE only need commit info on edits...
            'commit_info' => (object) ['author' => 'resume@danielbaer.com', 'message' => 'BaerCubs Minecraft'],
            'query'       => (object) array_merge($queryBase, $queryParts),
        ];
        // dump($queryFull); // #HERE
        $queryEncoded = json_encode($queryFull, JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);
        // dump($queryEncoded); // #HERE

        try {
            $response = $this->client->request('POST', "/api/woql/admin/$this->database", [
                'headers' => ['Content-Type' => 'application/json'],
                'body'    => $queryEncoded,
            ]);

            $statusCode = $response->getStatusCode();
            if ($statusCode !== 200) {
                // dump($statusCode, $response->getContent(false)); // #HERE
                $this->logger->critical("HTTP status code $statusCode returned when querying database.");

                return null;
            }

            $responseArray = $response->toArray();

            if ($responseArray['api:status'] === 'api:success') {
                return $responseArray['bindings'] ?: null;
            }
        } catch (TransportExceptionInterface $e) {
            $this->logger->critical('Transport exception when querying database.');
        }

        return null;
    }

    /**
     * Sets Symfony HttpClient instance for connecting to TerminusDB with.
     */
    public function setClient(
        ?string $database = null,
        ?string $key = null,
        ?string $host = null,
        ?string $port = null
    ): void {
        $database ??= Env::get('TERMINUSDB_DATABASE');
        $key      ??= Env::get('TERMINUSDB_API_KEY');
        $host     ??= Env::get('TERMINUSDB_HOST', 'localhost');
        $port     ??= Env::get('TERMINUSDB_PORT', '6363');

        $baseURI = "https://$host:$port";

        if ($database) {
            $this->database = $database;
        }

        if ($key) {
            $this->client = HttpClient::createForBaseUri(
                $baseURI,
                [
                    'auth_basic'  => ['admin', $key],
                    'verify_host' => false,
                    'verify_peer' => false,
                ]
            );
        }
    }

    /**
     * Sets a logger instance on the object.
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}
