<?php

namespace TerminusDB;

class Env {
    /**
     * Returns environment variable or value specified.
     *
     * @param string     $var     Environment variable to retrieve
     * @param mixed|null $default (default: null)
     *
     * @return mixed
     */
    public static function get( string $var, mixed $default = null ): mixed
    {
        return $_ENV[ $var ] ?? $default;
    }
}
